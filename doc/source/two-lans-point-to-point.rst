Example to demonstrate a point-to-point network that connect two LANS, LAN 1 
with ``n2, n3, n4, n5`` and LAN 2 with ``n6, n7, n8, n9`` via two routers ``n0``
and ``n1``. The nodes are given IPv4 addresses. The link has asymmetric bandwidth
and delay.
A ping application is used to test connectivity of the nodes.

Overview
++++++++
.. literalinclude:: two-lans-point-to-point.cc
   :language: cpp
   :start-after: start-general-documentation
   :end-before: end-general-documentation

Code
++++

.. literalinclude:: two-lans-point-to-point.cc
   :linenos:
   :language: cpp
   :start-after: code-body