This example extends from two-lans-point-to-point.cc to show how command line 
arguments and tracing can be utilized.

The program sets up a command-line argument parser to allow users to specify 
simulation parameters via command-line arguments and also configures logging
to capture and display log messages during the simulation.
 
The program also demonstrates how tracing works. Trace sources is connected
to trace sinks and analysed later. This program records tracing in pcap
format, ASCII format and console output. 

Overview
++++++++
.. literalinclude:: two-lans-tracing.cc
   :language: cpp
   :start-after: start-general-documentation
   :end-before: end-general-documentation

Code
++++

.. literalinclude:: two-lans-tracing.cc
   :linenos:
   :language: cpp
   :start-after: code-body