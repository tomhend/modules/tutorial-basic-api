This is an example to demonstrate point-to-point networks between two hosts 
``n0`` and ``n1``. The nodes are given IPv4 addresses. The link has asymmetric 
bandwidth and delay. A ping application is used to test connectivity of the 
nodes.

Overview
++++++++

.. literalinclude:: point-to-point-two-nodes.cc
   :language: cpp
   :start-after: start-general-documentation
   :end-before: end-general-documentation

Code
++++

.. literalinclude:: point-to-point-two-nodes.cc
   :linenos:
   :language: cpp
   :start-after: code-body
