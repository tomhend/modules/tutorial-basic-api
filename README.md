# Tutorial on ns-3 basic API #

This is an [ns-3](https://www.nsnam.org "ns-3 Website") tutorial covering
the basic API of ns-3 on simple wired (point-to-point and CSMA) topologies.

## Installation

The current repository is a development repository that is designed to be
cloned and installed as a
[contrib module](https://www.nsnam.org/docs/manual/html/new-modules.html).

```
cd contrib
git clone https://gitlab.com/tomhend/modules/tutorial-basic-api.git
```

Eventually, it is expected that this tutorial will be added to the
mainline ns-3 repository under the ``doc/`` directory.

## Tutorial programs

All programs are built as example programs, and are found in the ``examples/``
sub-directory.

## Tutorial documentation

As long as the
[system prerequisites](https://www.nsnam.org/docs/installation/html/index.html) 
for building documentation are available, the documentation
(PDF and HTML) can be built as follows:

```
cd doc
make latexpdf
make html
```
