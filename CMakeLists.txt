set(examples_as_tests_sources)
if(${ENABLE_EXAMPLES})
    set(examples_as_tests_sources
        #test/mod-examples-test-suite.cc
        )
endif()

build_lib(
    LIBNAME tutorial-basic-api
    SOURCE_FILES model/tutorial-basic-api.cc
    HEADER_FILES model/tutorial-basic-api.h
    LIBRARIES_TO_LINK ${libcore}
)

