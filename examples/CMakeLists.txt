build_lib_example(
  NAME point-to-point-two-nodes
  SOURCE_FILES point-to-point-two-nodes.cc
  LIBRARIES_TO_LINK
    ${libcore}
    ${libnetwork}
    ${libpoint-to-point}
    ${libinternet}
    ${libinternet-apps}
)

build_lib_example(
  NAME point-to-point-three-nodes
  SOURCE_FILES point-to-point-three-nodes.cc
  LIBRARIES_TO_LINK
    ${libcore}
    ${libnetwork}
    ${libpoint-to-point}
    ${libinternet}
    ${libinternet-apps}
)

build_lib_example(
  NAME point-to-point-four-nodes
  SOURCE_FILES point-to-point-four-nodes.cc
  LIBRARIES_TO_LINK
    ${libcore}
    ${libnetwork}
    ${libpoint-to-point}
    ${libinternet}
    ${libinternet-apps}
)

build_lib_example(
  NAME two-lans-connected-directly
  SOURCE_FILES two-lans-connected-directly.cc
  LIBRARIES_TO_LINK
    ${libcore}
    ${libnetwork}
    ${libcsma}
    ${libinternet}
    ${libinternet-apps}
)

build_lib_example(
  NAME two-lans-point-to-point
  SOURCE_FILES two-lans-point-to-point.cc
  LIBRARIES_TO_LINK
    ${libcore}
    ${libnetwork}
    ${libcsma}
    ${libpoint-to-point}
    ${libinternet}
    ${libinternet-apps}
)

build_lib_example(
  NAME two-lans-tracing
  SOURCE_FILES two-lans-tracing.cc
  LIBRARIES_TO_LINK
    ${libcore}
    ${libcsma}
    ${libinternet-apps}
    ${libinternet}
    ${libpoint-to-point}
)
